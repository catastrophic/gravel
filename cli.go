package main

import (
	"fmt"

	docopt "github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	usage = `
	 __   __             ___      
	/ _  |__)  /\  \  / |__  |
	\__> |  \ /~~\  \/  |___ |___


Description:
    
	gravel will try to find the ISBN from an epub, and optionally open its GoodReads page.
	
Usage:
    gravel [-g] [-c] <EPUB>...

Options:
    -g                     Open the GoodReads page, if an ISBN is found.
    -c                     Copy ISBN to clipboard, if found.
    -h, --help             Show this screen.
    --version              Show version.
`
	fullName    = "gravel"
	fullVersion = "%s -- %s"
)

var (
	Version               = "dev"
	ErrorEpubDoesNotExist = errors.New("epub does not exist")
)

type gravelArgs struct {
	builtin   bool
	goodreads bool
	clipboard bool
	epubFile  []string
}

func (b *gravelArgs) parseCLI(osArgs []string) error {
	// parse arguments and options
	args, err := docopt.Parse(usage, osArgs, true, fmt.Sprintf(fullVersion, fullName, Version), false, false)
	if err != nil {
		return errors.Wrap(err, "incorrect arguments")
	}
	if len(args) == 0 {
		// builtin command, nothing to do.
		b.builtin = true
		return nil
	}

	b.epubFile = args["<EPUB>"].([]string)
	for _, e := range b.epubFile {
		if !fs.FileExists(e) {
			return fmt.Errorf("%w: %q", ErrorEpubDoesNotExist, e)
		}
	}
	b.goodreads = args["-g"].(bool)
	b.clipboard = args["-c"].(bool)
	return nil
}
