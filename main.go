package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/atotto/clipboard"
	"github.com/skratchdot/open-golang/open"
	"gitlab.com/catastrophic/assistance/epub"
	"gitlab.com/catastrophic/assistance/ui"
)

func main() {
	// parsing CLI
	cli := &gravelArgs{}
	if err := cli.parseCLI(os.Args[1:]); err != nil {
		fmt.Println(err.Error())
		return
	}
	if cli.builtin {
		return
	}

	// try to find a GoodReads Dev API Key in env.
	// get one at https://www.goodreads.com/api/keys
	key := os.Getenv("GR_API_KEY")

	for _, ep := range cli.epubFile {
		var e *epub.Epub
		var err error

		// open epub
		if key == "" {
			e, err = epub.New(ep)
		} else {
			e, err = epub.NewWithGoodReads(ep, key)
		}
		if err != nil {
			fmt.Println(ui.RedBold(" ● ") + ui.Blue(filepath.Base(ep)) + ": " + ui.RedBold(err.Error()))
			continue
		}
		// parse its metadata
		if err := e.Parse(); err != nil {
			fmt.Println(ui.RedBold(" ● ") + ui.Blue(filepath.Base(ep)) + ": " + ui.RedBold(err.Error()))
			continue
		}
		// gather information including ISBN
		if err := e.ReadMetadata(true, false); err != nil {
			fmt.Println(ui.RedBold(" ● ") + ui.Blue(filepath.Base(ep)) + ": " + ui.RedBold(err.Error()))
			continue
		}
		// results
		if e.Metadata.ISBN == "" {
			fmt.Println(ui.RedBold(" ● ") + ui.Blue(filepath.Base(ep)) + ": " + ui.RedBold("no ISBN found in this epub"))
			continue
		}
		fmt.Println(ui.GreenBold(" ◯ ") + ui.Blue(filepath.Base(ep)) + ": " + ui.GreenBold(e.Metadata.ISBN))
		// optionally opening GR
		if cli.goodreads {
			if err := open.Start("https://www.goodreads.com/search?q=" + e.Metadata.ISBN); err != nil {
				fmt.Println("Error: could not open goodreads page for " + filepath.Base(ep))
			}
		}
		//optionally copying to clipboard
		if cli.clipboard {
			if err := clipboard.WriteAll(e.Metadata.ISBN); err != nil {
				fmt.Println("Error: could not copy ISBN to clipboard: " + err.Error())
			} else {
				fmt.Println("ISBN copied to clipboard.")
			}
		}
	}
}
