GO = GO111MODULE=on go
VERSION=`git describe --tags`

all: fmt check test-coverage build

prepare:
	${GO} get -u github.com/divan/depscheck
	${GO} get github.com/warmans/golocc
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin v1.27.0

deps:
	${GO} mod download

fmt:
	${GO} fmt ./...

check: fmt
	golangci-lint run

info: fmt
	depscheck -totalonly -tests .
	golocc .

test:
	${GO} test  -v ./... -cover

test-coverage:
	${GO} test -race -coverprofile=coverage.txt -covermode=atomic ./...

clean:
	rm -f coverage.txt
	rm -f gravel.exe
	rm -f gravel

build:
	${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o gravel -v ./....
	GOOS=windows GOARCH=386 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o gravel.exe -v ./...

install: build
	mv gravel ${GOPATH}/bin





