module gitlab.com/catastrophic/gravel

go 1.14

require (
	github.com/atotto/clipboard v0.1.2
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/pkg/errors v0.8.1
	github.com/skratchdot/open-golang v0.0.0-20190104022628-a2dfa6d0dab6
	gitlab.com/catastrophic/assistance v0.36.2
)

// replace gitlab.com/catastrophic/assistance => ../../catastrophic/assistance
