![gravel](https://i.imgur.com/76Mjz1M.png)

# gravel

[![Documentation](https://godoc.org/gitlab.com/catastrophic/gravel?status.svg)](https://godoc.org/gitlab.com/catastrophic/gravel)
 [![Go Report Card](https://goreportcard.com/badge/gitlab.com/catastrophic/gravel)](https://goreportcard.com/report/gitlab.com/catastrophic/gravel) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Build status](https://gitlab.com/catastrophic/gravel/badges/master/build.svg)](https://gitlab.com/catastrophic/gravel/pipelines)

Epub metadata is an excellent idea suffering from terrible execution by quite a sizeable portion of publishers. One of its most important fields, ISBN, can be found in a variety of places; sometimes it is only mentioned in the text itself. 

This is a very simple tool that looks everywhere in the Epub file. Once the ISBN is found, it can also automatically open its GoodReads page. 

## usage

`$ gravel -h`:

         __   __             ___      
        / _  |__)  /\  \  / |__  |
        \__> |  \ /~~\  \/  |___ |___
    
    
    Description:
        
        gravel will try to find the ISBN from an epub, and optionally open its GoodReads page.
        
    Usage:
        gravel [-g]  <EPUB>...
    
    Options:
        -g                     Open the GoodReads page, if an ISBN is found.
        -h, --help             Show this screen.
        --version              Show version.


If you have a Goodreads API Key (and you can get one [here](https://www.goodreads.com/api/keys)), you can set the `GR_API_KEY` environment variable to have `gravel` help you choose when an Epub contains mentions of several ISBNs:

`$ env GR_API_KEY=THEACTUALKEY gravel *.epub`

If `gravel` finds more than one candidate, it will provide a choice. 
* If the Goodreads API key is set, each candidate will have a quick description (after GR:) of the book which it refers
* Each candidate will have a list of places where it was found:
    * `OPF` or `NCX` means the candidate was found in a relatively nominal location in the metadata.
    * Other files are also analyzed (filenames & contents) and can contain ISBNs.
* Each candidate will provide some context of where it was found (`...context...`). 
All of this combined should help you find the right candidate.

## example

(environment contains the Goodreads API key)

![gravel](https://i.imgur.com/Ws0BoA9.png)

## failure is always an option

Sometimes nothing is found. Some epubs really are awfully made. If, however, you can find a rogue epub that you are sure contains a valid ISBN number, somewhere, that `gravel` does not detect, please let me know.
